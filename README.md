# mega_zh-TW

整合各項常用 MOD 正體中文化資料，此MOD用於自行更新用。

### 使用方法
自行下載解壓縮之後，將擁有MOD的資料夾覆蓋，並檢查其中是否有檔名不符合的部分，如有，直接更改檔案名稱。

接著至個別的 module.json 中的 "languages" 加上以下內容，如有 zh-tw 或其他相似，但不同的字串，請一律改為以下，因 FVTT 本身 "尚未" 統一大小寫格式，會導致有多個同種語系出現，並且有極大的可能導致顯示仍舊呈現為英文。

    {
      "lang": "zh-TW",
      "name": "正體中文",
      "path": "./lang/zh-TW.json"
    }


### 主要使用系統
* [Translation: 正體中文 [FVTT_Core]](https://foundryvtt.com/packages/foundry_zh_TW)
* [Translation: 正體中文 [DnD5E]](https://foundryvtt.com/packages/5e_zh_TW)

### 已包含 MOD 清單

* [Crash's Tracking & Training (5e)](https://foundryvtt.com/packages/5e-training)
* [Chat Images](https://foundryvtt.com/packages/chat-images)
* [Combat Utility Belt](https://foundryvtt.com/packages/combat-utility-belt)
* [Dynamic Active Effects](https://foundryvtt.com/packages/dae)
* [Dice So Nice!](https://foundryvtt.com/packages/dice-so-nice)
* [Forien's Quest Log](https://foundryvtt.com/packages/forien-quest-log)
* [FXMaster](https://foundryvtt.com/packages/fxmaster)
* [Health Estimate](https://foundryvtt.com/packages/healthEstimate)
* [Midi Quality of Life Improvements](https://foundryvtt.com/packages/midi-qol)
* [Monk's TokenBar](https://foundryvtt.com/packages/monks-tokenbar)
* [Tabbed Chatlog](https://foundryvtt.com/packages/tabbed-chatlog)
* [Tidy5e Sheet](https://foundryvtt.com/packages/tidy5e-sheet)
* [Token Action HUD](https://foundryvtt.com/packages/token-action-hud)
* [Token Magic FX](https://foundryvtt.com/packages/tokenmagic)
* [Turn Marker](https://foundryvtt.com/packages/turnmarker)
* [About Time](https://foundryvtt.com/packages/about-time) 通常會作為 Calendar/Weather 的附件安裝 
* [Calendar/Weather](https://foundryvtt.com/packages/calendar-weather)

## 鳴謝
[FoundryVTT 中文社区](https://github.com/fvtt-cn)
以及其他翻譯的前者們。

## 其他問題
如果對於翻譯有需要校對，或是有什麼額外的MOD希望引入的。
歡迎直接在 issue 中提出。